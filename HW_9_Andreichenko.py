# Создайте класс "Транспортное средство" и отнаследуйтесь от него классами "Самолет", "Автомобиль", "Корабль".
# Продумайте и реализуйте в классе "Транспортное средство" общие атрибуты для "Самолет", "Автомобиль", "Корабль".
# В наследниках реализуйте характерные для них атрибуты и методы

import math

class Transport:
    name = ""
    type = ""
    color = ""
    speed = 0
    number_of_seats = 0
    lift_capacity = 0.0 # in Kg

    def __init__(self, name: str, type: str, color: str, speed: int, number_of_seats: int, lift_capacity: float):
        self.name = name
        self.type = type
        self.color = color
        self.speed = speed
        self.number_of_seats = number_of_seats
        self.lift_capacity = lift_capacity

    def move(self, time: int):
        if isinstance(time, (int,float)) and isinstance(self.speed, (int,float)):
            return time * self.speed
        else:
            raise TypeError

    def loadCargo(self, cargoweight: float):
        if isinstance(cargoweight, (int,float)):
            if cargoweight > self.lift_capacity:
                return "Cannot load - too much weights!"
            else:
                return "Successfully loaded !"
        else:
            raise TypeError



class Automobile(Transport):
    brand = ""
    drive_unit = ""
    fuel_volume = 0.0 # in liters
    brake_fluid_percentage = 0.0

    def __init__(self, name: str, type: str, color: str, speed: int, number_of_seats: int, lift_capacity: float, brand: str, drive_unit: str,  fuel_volume: float , brake_fluid_percentage: float):
        self.name = name
        self.type = type
        self.color = color
        self.speed = speed
        self.number_of_seats = number_of_seats
        self.lift_capacity = lift_capacity
        self.brand = brand
        self.drive_unit = drive_unit
        self.fuel_volume = fuel_volume
        self.brake_fluid_percentage = brake_fluid_percentage

    def Drif(self):
        tolower = self.drive_unit.lower()
        if tolower == "front drive":
            return "Reduce the traction of the rear wheels."
        elif tolower == "back drive":
            return "Back drive is ideal for drifting. The hand brake will also help you with this."
        elif tolower == "full drive":
            return "Determine what type of drive unit you will have during drifting."
        else:
            return "You are going to crash!"



class Airplane(Transport):
    fuel_volume = 0.0 # in liters
    engines_count = 0
    maximum_flight_altitude = 0.0 # in Km
    autopilot_from_Tesla = False
    can_take_off_land_and_water = False

    def __init__(self, name: str, type: str, color: str, speed: int, number_of_seats: int, lift_capacity: float, fuel_volume: float, engines_count: int, maximum_flight_altitude: float, autopilot_from_Tesla: bool, can_take_off_land_and_water: bool):
        self.name = name
        self.type = type
        self.color = color
        self.speed = speed
        self.number_of_seats = number_of_seats
        self.lift_capacity = lift_capacity
        self.fuel_volume = fuel_volume
        self.engines_count = engines_count
        self.maximum_flight_altitude = maximum_flight_altitude
        self.autopilot_from_Tesla = autopilot_from_Tesla
        self.can_take_off_land_and_water = can_take_off_land_and_water

    def TakeOff(self, angle: int, strip_length: float):
        if isinstance(angle, int) and isinstance(strip_length, (int,float)):
            if strip_length < 500:
                return "Strip is less than 500 meters. We can not start flight!"
            if angle < 15 or angle > 60:
                return "The plane crashes with wrong angles"
            else:
                return "Ready to take off!"
        else:
            raise TypeError


    def Flight(self, from_place: str, to_place: str):
        if self.autopilot_from_Tesla:
            return f"The Airplane goes from {from_place} to {to_place} with autopilot"
        else:
            return f"The Airplane goes from {from_place} to {to_place} without autopilot"


class Ship(Transport):
    waterline = ""
    stability = 0.0
    sail_count = 0

    def __init__(self, name: str, type: str, color: str, speed: int, number_of_seats: int, lift_capacity: float, waterline: str, sail_count: int):
        self.name = name
        self.type = type
        self.color = color
        self.speed = speed
        self.number_of_seats = number_of_seats
        self.lift_capacity = lift_capacity
        self.waterline = waterline
        self.sail_count = sail_count


    def defineStability(self, ship_weight: float, angle: float):
        if isinstance(ship_weight,(int,float)) and isinstance(angle, (int,float)):
            return ship_weight * math.sin(angle)
        else:
            raise TypeError


def main():
    # transp = Transport("Cart", "land", "black", 10, 5, 100.5)
    # print(transp.loadCargo(23.5))

    print("#######")
    car = Automobile("Subaru", "land", "blue", 200, 9, 500, "BMV", "back drive", 55.6, 100)
    print(car.Drif())

    print("#######")
    airplane = Airplane("Ark", "aerial", "white", 600, 49, 110000, 48000, 4, 100, True, False)
    print(airplane.loadCargo(6000))
    print(airplane.TakeOff(15, 550))
    print(airplane.Flight("New York", "Kyiv"))

    print("#######")
    ship = Ship("Red Wing", "marine", "red", 50, 30, 5000, "S - Summer Seawater", 5)
    print("Ship Stability =", ship.defineStability(18000, math.pi / 6))



main()











